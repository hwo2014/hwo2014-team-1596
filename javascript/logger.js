//logger.js
module.exports = {
  log: function(data) {
    var msg = data.msgType;
    if (msg === 'carPositions') {

    } else if (msg === 'join') {
        console.log('Joined')
    } else if (msg === 'gameStart') {
      console.log('Race started');
    } else if (msg === 'gameInit') {
      var info = 'Game init. Track info:';
      var trackInfo = data.data.race.track;
      info += '\nTrack name: ' + trackInfo.name;
      info += '. Total pieces: ' + trackInfo.pieces.length;
      info += '. Total lanes: ' + trackInfo.lanes.length;
      info += '\nPieces:';
      trackInfo.pieces.forEach(function(piece, i) {
        info += '\nPiece ' + i + ':';
        if(piece.length) {
          info += ' Length ' + piece.length;
        }
        if(piece.switch) {
          info += ' with switch';
        }
        if(piece.radius) {
          info += ' Radius ' + piece.radius;
        }
        if(piece.angle) {
          info += ' Angle ' + piece.angle;
        }
      });
      info += '\nLanes:'
      trackInfo.lanes.forEach(function(lane) {
        info += '\nLane ' + lane.index + " distance from center " + lane.distanceFromCenter;
      });
      console.log(info);
    } else if (msg === 'gameEnd') {
      console.log('Race ended');
    } else if (msg === 'crash') {
      console.log('Crash');
    } else if (msg === 'spawn') {
      console.log('Spawn');
    } else if (msg === 'lapFinished') {
      console.log('Lap ' + data.data.lapTime.lap + ' Finished in ' + data.data.lapTime.millis + 'ms');
    } else if (msg === 'dfn') {
      console.log('Disqualified');
    } else if (msg === 'finish') {
      console.log('Finished race');
    } else {
      console.log(msg);
    }

  }
}
