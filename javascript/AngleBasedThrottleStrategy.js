var trackInfo, car, maxSpeed;
var lastDistance;

module.exports = {

	init: function(trackInfo, car) {
		this.trackInfo = trackInfo;
		this.car = car;
		this.lastDistance = 0;
	},

	isDriftingInOrBeforeABend: function(piecePosition) {
		var nextPiece = this.trackInfo.nextPieceIndex(piecePosition.pieceIndex);
		return (this.car.isDrifting() && (this.trackInfo.isBend(piecePosition.pieceIndex) || this.trackInfo.isBend(nextPiece)));
	},

	getNewThrottle: function(piecePosition) {
	    var nextPiece = this.trackInfo.nextPieceIndex(piecePosition.pieceIndex);
	    var throttle = 1.0;

	    if (piecePosition.inPieceDistance > this.lastDistance) {
	      this.car.speed = piecePosition.inPieceDistance - this.lastDistance;
	    }

	    this.lastDistance = piecePosition.inPieceDistance;    
	    this.maxSpeed = 10;

	    if (this.trackInfo.isBend(nextPiece) && Math.abs(this.trackInfo.getAngle(nextPiece)) > 30) {
	      this.maxSpeed = 7.5;
	    }

	    if (this.isDriftingInOrBeforeABend(piecePosition) || this.car.speed > this.maxSpeed) {
	      throttle = 0;
	    }

	    if (this.trackInfo.isBend(piecePosition.pieceIndex) && !this.car.isDrifting() && this.car.angle < 57) {
	      throttle = this.car.throttle + 0.4;
	    }    

	    if (throttle > 1.0) {
	    	throttle = 1.0;
	    }

	    return throttle;
	}

}