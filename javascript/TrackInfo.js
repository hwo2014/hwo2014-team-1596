//TrackInfo.js
var info, cars, raceSession;
var bends = [];

module.exports = {
  setInfo: function(data) {
    this.info = data.data.race.track;
    this.cars = data.data.race.cars;
    this.raceSession = data.data.race.raceSession;
    this._fillBendInfo();
    console.log(bends);
  },

  nextBendIndex: function(index) {
      var nextIndex = index;

      if (this.isBend(index)) {
          nextIndex = this._skipBend(index);
      }

      if (this.isStraight(nextIndex)) {
          nextIndex = this._skipStraight(nextIndex);
      }

      return nextIndex;
  },

  _fillBendInfo: function() {
      var firstBendIndex = 0;

      if (!this.isBend(firstBendIndex)) {
          firstBendIndex = this.nextBendIndex(firstBendIndex);
      } else {
          bends.push({
              pieceIndex: firstBendIndex,
              maximumCarAngle: 0,
              carLane: 0,
              speedCorrection: 0
          });
      }

      var nextIndex = this.nextBendIndex(firstBendIndex);
      while (nextIndex > firstBendIndex) {
          bends.push({
              pieceIndex: nextIndex,
              maximumCarAngle: 0,
              carLane: 0,
              speedCorrection: 0
          });

          console.log(nextIndex + ' - ' + firstBendIndex);
          nextIndex = this.nextBendIndex(nextIndex);
      }
  },

  updateAngle: function(angle, index, lane) {
      console.log('    updating angle for ' + index + ' and lane ' +  lane);

      var bendIndex = this.getRelatedBendForIndex(index);
      if (bends[bendIndex].maximumCarAngle < Math.abs(angle)) {
          bends[bendIndex].maximumCarAngle = Math.abs(angle);
      }
  },

  crashed: function(index, lane) {
      console.log('*** CRASHED at ' +  index);

      var bendIndex = this.getRelatedBendForIndex(index);
      bends[bendIndex].crashed = true;
  },

  getSpeedCorrection: function(bendIndex, lane) {
      var speedCorrection = 0;
      bends.forEach(function(bend) {
          if (bend.pieceIndex == bendIndex && bend.carLane == lane) {
              speedCorrection = bend.speedCorrection;
          }
      });
      return speedCorrection;
  },

  updateBendInfo: function() {
      bends.forEach(function(bend) {
          if (bend.crashed) {
              bend.speedCorrection = bend.speedCorrection - 0.1;
              bend.crashed = false;
          } else if (Math.abs(bend.maximumCarAngle) < 55) {
              bend.speedCorrection = bend.speedCorrection + ((1 / (Math.abs(bend.maximumCarAngle) + 1)) - 1 / 59);
          }
      });

      console.log(bends);
  },

  isStraight: function(pieceIndex) {
    if(pieceIndex || pieceIndex === 0) {
      return this.info.pieces[pieceIndex].length !== undefined;
    }
    return false;
  },

  isBend: function(pieceIndex) {
    if(pieceIndex || pieceIndex === 0) {
      return this.info.pieces[pieceIndex].angle !== undefined;
    }
    return false;
  },

  isSwitch: function(pieceIndex) {
    return this.info.pieces[pieceIndex].switch || false;
  },

  getAngle: function(pieceIndex) {
    if(!this.isBend(pieceIndex)) {
      return 0;
    }
    return this.info.pieces[pieceIndex].angle;
  },

  getRadius: function(pieceIndex) {
    if(!this.isBend(pieceIndex)) {
      return 0;
    }
    return this.info.pieces[pieceIndex].radius;
  },

    getRadiusForLane: function(pieceIndex, laneIndex) {
        if(!this.isBend(pieceIndex)) {
            return 0;
        }

        var laneDistanceFromCenter = this.info.lanes[laneIndex].distanceFromCenter;
        return this.info.pieces[pieceIndex].radius - laneDistanceFromCenter;
    },

  nextPieceIndex: function(pieceIndex) {
    pieceIndex++;
    if (pieceIndex < 0 || pieceIndex >= this.info.pieces.length) {
      return 0;
    }
    return pieceIndex;
  },

  getPieceLength: function (pieceIndex, lane) {
    if(this.isStraight(pieceIndex)) {
      return this.info.pieces[pieceIndex].length;
    }else {
      var piece = this.info.pieces[pieceIndex];
      var laneCorrection = this.info.lanes[lane].distanceFromCenter;
      if(piece.angle > 0) {
        laneCorrection *= -1;
      }
      var radius = piece.radius + laneCorrection;
      var length = (2 * Math.PI * radius) * (Math.abs(piece.angle)/360);
      return length;
    }
  },

  getNextBendDirection: function(pieceIndex) {
    var nextBend = this.getNextBendIndex(pieceIndex);
    return this.getBendDirection(nextBend);
  },

  getBendDirection: function(pieceIndex) {
    var angle = this.getAngle(pieceIndex);
    if(angle < 0) {
      return 'Left';
    }
    if(angle > 0) {
      return 'Right';
    }
  },

  getRelatedBendForIndex: function(pieceIndex) {
      if (pieceIndex < 0 || pieceIndex >= this.info.pieces.length) {
          return 0;
      }

      for (var bendIndex = 0; bendIndex < bends.length; bendIndex++) {
          if (bends[bendIndex].pieceIndex > pieceIndex) {
              if (bendIndex == 0) {
                  return bends.length - 1;
              } else {
                  return bendIndex - 1;
              }
          }
      }

      return bends.length - 1;
  },

  getNextBendIndex: function(pieceIndex) {
    var nextBend = this.nextPieceIndex(pieceIndex);

    while(this.isStraight(nextBend) && nextBend !== pieceIndex) {
      nextBend = this.nextPieceIndex(nextBend);
    }

    return nextBend;
  },

  getNextSwitchIndex: function(pieceIndex) {
    var nextSwitch = this.nextPieceIndex(pieceIndex);
    while(!this.isSwitch(nextSwitch) && nextSwitch !== pieceIndex) {
      nextSwitch = this.nextPieceIndex(nextSwitch);
    }
    return nextSwitch;
  },

  isALaneOn: function(direction, actualLane) {
    if(direction === 'Right') {
      return (this.info.lanes.length - 1) > actualLane;
    } else if (direction === 'Left') {
      return actualLane > 0;
    }
  },

  getNumberOfLanes: function() {
    return this.info.lanes.length; 
  },

  getTotalLength: function(lane) {
    var total = 0;
    for(var i=0; i<this.info.pieces.length; i++) {
      console.log('longitud pieza '+ i + ': ' + this.getPieceLength(i,lane));
      total += this.getPieceLength(i,lane);
    }
    return total;
  },

  getDistanceBetween: function(piece, otherPiece, lane ) {
    var distance = 0;
    var startIndex = piece;

    while (startIndex != otherPiece) {
        distance += this.getPieceLength(startIndex, lane);
        startIndex = this.nextPieceIndex(startIndex);
    }

    return distance;
  },

  _skipBend: function(pieceIndex) {
      var currentPieceAngle = this.getAngle(pieceIndex);
      while (this.getAngle(pieceIndex) == currentPieceAngle) {
          pieceIndex = this.nextPieceIndex(pieceIndex);
      }

      return pieceIndex;
  },

  _skipStraight: function(pieceIndex) {
      while (this.isStraight(pieceIndex)) {
          pieceIndex = this.nextPieceIndex(pieceIndex);
      }

      return pieceIndex;
  },

  getNextBendInfo: function(piece, lane) {
    var result = {
      index: piece,
      distance: 0
    };

    result.index = this.nextBendIndex(result.index);
    result.distance = this.getDistanceBetween(piece, result.index, lane);

    return result;
  },

  isQuickRace: function() {
    return this.raceSession.quickRace || false;
  },

  isQualifying: function() {
    return this.raceSession.durationMs !== undefined;
  },

  isRace: function() {
    return this.raceSession.laps !== undefined && !this.isQuickRace();
  }
}
