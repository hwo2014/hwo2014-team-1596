var carPositionInfo, trackInfo;
var reached;

module.exports = {
	init: function(trackInfo, carPositionInfo) {
		this.trackInfo = trackInfo;
        this.carPositionInfo = carPositionInfo;
		this.reached = false;
	},

	getNewThrottle: function() {

        var carInfo = this.carPositionInfo.getCarInfo(this.carPositionInfo.myCarName);
		var nextPiece = this.trackInfo.nextPieceIndex(carInfo.pieceIndex);
		console.log(this.reached);

		if (carInfo.speed > 4) {
			this.reached = true;
			return 0;
		}

	    // if (this.trackInfo.isBend(nextPiece)) {
	    //   return 0.4;
	    // }

	    if (this.reached) {
	    	return 0;
	    }

	    return 1
	},

    mayRunTurbo: function(pieceIndex, lane) {
        return false;
    }
}