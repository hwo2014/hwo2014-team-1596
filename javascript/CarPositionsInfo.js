//CarPositionInfo.js
var info, previousInfo;
var myCarName;
var trackInfo;
var throttle = 0;
var frictionCoeficient = 0.00408;

module.exports = {

  setInfo: function (data) {
    this.previousInfo = this.info || {};
    this.info = {};

    var self = this;
    data.forEach(function(item) {
      var carSpeed = self._calculateSpeed(item.id.name, item.piecePosition);
      var carAcceleration = self._calculateAcceleration(item.id.name, carSpeed);
      var driftIncrease = self._calculateDriftIncrease(item.id.name, item.angle);
      var driftVariation = self._calculateDriftVariation(item.id.name, item.angle);

      var previousCarInfo = self.getPreviousCarInfo(item.id.name);
      var turboInfo = previousCarInfo.turbo || false;
      
      if (turboInfo) {
        turboInfo.turboDurationTicks--;
        if (turboInfo.turboDurationTicks == 0) {
          turboInfo = false;
        }
      }

      var carInfo = {
        color: item.id.color, 
        speed: carSpeed,
        acceleration: carAcceleration,
        driftIncrease: driftIncrease,
        driftVariation: driftVariation,
        turbo: turboInfo,
        angle: item.angle,
        lap: item.piecePosition.lap,
        pieceIndex: item.piecePosition.pieceIndex,
        inPieceDistance: item.piecePosition.inPieceDistance,
        startLane: item.piecePosition.lane.startLaneIndex,
        endLane: item.piecePosition.lane.endLaneIndex
      };

      if (item.id.name == self.myCarName) {
          carInfo.throttle = self.throttle;
      }

      self.info[item.id.name] = carInfo;
    });

    this.previousInfo = this.previousInfo || this.info;
  },

  _calculateSpeed: function(name, piecePosition) {
    var previousCarInfo = this.getPreviousCarInfo(name);

    if (!previousCarInfo.inPieceDistance) {
      previousCarInfo.inPieceDistance = 0;
    }

    if (piecePosition.inPieceDistance >= previousCarInfo.inPieceDistance) {
      return piecePosition.inPieceDistance - previousCarInfo.inPieceDistance;
    } 

    var lastPieceRemainingDistance = this.trackInfo.getPieceLength(previousCarInfo.pieceIndex, previousCarInfo.endLane) -
      previousCarInfo.inPieceDistance;

    return piecePosition.inPieceDistance + lastPieceRemainingDistance; 
  },

  _calculateAcceleration: function(name, newSpeed) {
    var previousCarInfo = this.getPreviousCarInfo(name);
    if (!previousCarInfo.speed) {
      previousCarInfo.speed = 0;
    }

    return newSpeed - previousCarInfo.speed;
  },

  _calculateDriftIncrease: function(name, angle) {
    var previousCarInfo = this.getPreviousCarInfo(name);

    if (!previousCarInfo.angle) {
      previousCarInfo.angle = 0;
    }

    if ((angle < 0 && angle < previousCarInfo.angle) ||
        (angle > 0 && angle > previousCarInfo.angle)) {      
          return Math.abs(previousCarInfo.angle - angle);
    }

    return 0;
  },

  _calculateDriftVariation: function(name, angle) {
    var previousCarInfo = this.getPreviousCarInfo(name);

    if (!previousCarInfo.angle) {
      previousCarInfo.angle = 0;
    }

    return Math.abs(angle - previousCarInfo.angle);
  },

  setMyCar: function(name) {
    this.myCarName = name;
  },

  setTrackInfo: function(trackInfo) {
    this.trackInfo = trackInfo;
    this.info = {};
    var carInfo = {
        color: '', 
        speed: 0,
        acceleration: 0,
        driftIncrease: 0,
        driftVariation: 0,
        turbo: false,
        angle: 0, 
        lap: 0,
        pieceIndex: 0,
        inPieceDistance: 0,
        startLane: 0,
        endLane: 0,
        throttle: 1.0
    };

    this.info['Articos RT'] = carInfo;
  },

  isDrifting: function(carName) {
    carName = carName || this.myCarName;
    var carInfo = this.getCarInfo(carName);
    return carInfo.driftVariation > 0;    
  },

  getAngle: function (carName) {
    carName = carName || this.myCarName;
    var carInfo = this.getCarInfo(carName);
    return carInfo.angle;
  },

  getPieceIndex: function(carName) {
    carName = carName || this.myCarName;
    var carInfo = this.getCarInfo(carName);
    return carInfo.pieceIndex;
  },

  getInPieceDistance: function(carName) {
    carName = carName || this.myCarName;
    var carInfo = this.getCarInfo(carName);
    return carInfo.inPieceDistance;
  },

  getActualLane: function(carName) {
    carName = carName || this.myCarName;
    var carInfo = this.getCarInfo(carName);
    return carInfo.endLane;
  },

  isChangingLane: function(carName) {
    carName = carName || this.myCarName;
    var carInfo = this.getCarInfo(carName);
    return carInfo.startLane !== carInfo.endLane;
  },

  getLastTickDistance: function (carName) {
    carName = carName || this.myCarName;
    var carInfo = this.getCarInfo(carName);
    var previousInfo = this.getPreviousCarInfo(carName);
    if(carInfo.pieceIndex === previousInfo.pieceIndex) {
      return carInfo.inPieceDistance - previousInfo.inPieceDistance;
    } else {
      var pieceLength = 100;
      if(this.trackInfo) {
        pieceLength = this.trackInfo.getPieceLength(previousInfo.pieceIndex, carInfo.endLane);
      }

      return pieceLength - previousInfo.inPieceDistance + carInfo.inPieceDistance;
    }
  },

  getCarInfo: function(carName) {
    return this.info[carName];
  },

  getPreviousCarInfo: function(carName) {
    if (!this.previousInfo) {
      return {};
    }

    return this.previousInfo[carName] || {};
  },

  setThrottle: function(carName, throttle) {
    this.throttle = throttle;
  },

  setTurbo: function(carName, turboInfo) {
    var carInfo = this.getCarInfo(carName);
    carInfo.turbo = turboInfo;
  },

  frictionForce: function(carName) {
    // Valores obtenidos empíricamente al resolver un sistema de ecuaciones de
    // segundo grado relacionando velocidad y fuerza de deceleración provocada
    // en el coche por la fricción.
    var carInfo = this.getCarInfo(carName);
    if (!carInfo.speed) {
      carInfo.speed = 0;
    }

    return 0.00041 * carInfo.speed;
  },

  distanceToAcelerateToSpeed: function(carName, targetSpeed) {
    var carInfo = this.getCarInfo(carName);

    var distanceToTarget = 0;
    if (carInfo.speed >= targetSpeed) {
      return distanceToTarget;
    }

    var initSpeed = this.speed;
    while (initSpeed < targetSpeed) {
      distanceToTarget += initSpeed;
      initSpeed += (this.frictionCoeficient - this.frictionForce(initSpeed));
    }

    return distanceToTarget;
  },

  distanceToDecelerateToSpeed: function(carName, targetSpeed) {
    var carInfo = this.getCarInfo(carName);

    var distanceToTargetSpeed = 0;
    console.log(carInfo.speed);

    if (carInfo.speed <= targetSpeed) {
      return distanceToTargetSpeed;
    }

    var affectingThrottle = carInfo.throttle;
    if (carInfo.acceleration > 0) {
        if (affectingThrottle == 0) {
            var previousCarInfo = this.getPreviousCarInfo(carName);
            affectingThrottle = previousCarInfo.throttle;
        }
    }

    var currentAccelerationDueToThrottle = 0.2 * affectingThrottle;
    if (carInfo.turbo) {
      currentAccelerationDueToThrottle *= carInfo.turbo.turboFactor;
    }
    var currentAcceleration = carInfo.acceleration - currentAccelerationDueToThrottle;

    console.log('initial acceleration: ' + currentAcceleration + ' with throttle affection ' + affectingThrottle);
    while (currentAcceleration > 0) {
      currentAcceleration -= currentAccelerationDueToThrottle;
    }

    var initSpeed = carInfo.speed + currentAcceleration;
    distanceToTargetSpeed = initSpeed;
    if (carInfo.throttle > 0) {
        distanceToTargetSpeed += carInfo.speed;
    }

    while (initSpeed > targetSpeed) {
      distanceToTargetSpeed += initSpeed;

      currentAcceleration += this.frictionForce(carName, initSpeed);
      if (currentAcceleration > -0.01) {
          currentAcceleration = -0.01;
      }

      initSpeed += currentAcceleration;
    }


    return distanceToTargetSpeed;
  }

}
