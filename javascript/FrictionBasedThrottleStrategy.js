var trackInfo, carPositionInfo, maxSpeed;
var carName = 'Articos RT';
var MAX_CENT_FORCE = 0.5;
var CRASH_ANGLE = 60;

module.exports = {

	init: function(trackInfo, carPositionInfo) {
		this.trackInfo = trackInfo;
		this.carPositionInfo = carPositionInfo;
		this.lastDistance = 0;
	},

	isDriftingInOrBeforeABend: function(pieceIndex) {
		var nextPiece = this.trackInfo.nextPieceIndex(pieceIndex);
		if (this.trackInfo.getRadius(pieceIndex) < 30) {
			return false;
		}

		return (this.carPositionInfo.isDrifting(carName) && (this.trackInfo.isBend(pieceIndex) || this.trackInfo.isBend(nextPiece)));
	},

	relativeAngleIncrease: function() {
		var carInfo = this.carPositionInfo.getCarInfo(carName);

		var driftIncrease = carInfo.driftIncrease;
        var angleFactor = Math.abs(carInfo.angle) / CRASH_ANGLE;

        return driftIncrease * angleFactor;
	},

    isCarCentForceWorrying: function(cent_force) {
        var carInfo = this.carPositionInfo.getCarInfo(carName);

        if (cent_force > MAX_CENT_FORCE && carInfo.driftVariation > 1) {
            return true;
        }

        return false;
    },

	getNewThrottle: function() {
	    var carInfo = this.carPositionInfo.getCarInfo(carName);
	    var throttle = carInfo.throttle;

	    this.maxSpeed = 20;	
	    
	    var nextBendWithDistance = this.trackInfo.getNextBendInfo(carInfo.pieceIndex, carInfo.startLane);
		var remainingPieceDistance = nextBendWithDistance.distance - carInfo.inPieceDistance;

		this.maxSpeedIfNextBend = Math.sqrt(MAX_CENT_FORCE *
            this.trackInfo.getRadiusForLane(nextBendWithDistance.index, carInfo.startLane)) +
            this.trackInfo.getSpeedCorrection(nextBendWithDistance.index, carInfo.endLane);
        if (remainingPieceDistance < 150) {
            this.maxSpeedIfNextBend -= carInfo.driftVariation;
        }

        console.log('    next bend: {' + nextBendWithDistance.index + ', ' + nextBendWithDistance.distance + '} in ' + remainingPieceDistance +
            ' at max speed ' + this.maxSpeedIfNextBend);
        console.log('    radius: ' + this.trackInfo.getRadius(nextBendWithDistance.index) + ' for lane: ' +
            this.trackInfo.getRadiusForLane(nextBendWithDistance.index, carInfo.startLane));

        console.log('    IS remaining distance: ' + remainingPieceDistance +
            ' LESS THAN distance to decelerate to ' + this.maxSpeedIfNextBend + '?: ' +
            this.carPositionInfo.distanceToDecelerateToSpeed(carName, this.maxSpeedIfNextBend));

        throttle = 1.0 - this.relativeAngleIncrease() / 2;

        if (this.carPositionInfo.distanceToDecelerateToSpeed(carName, this.maxSpeedIfNextBend) >= remainingPieceDistance) {
            throttle = 0;
        }

        if (this.trackInfo.isBend(carInfo.pieceIndex)) {
		    var cent_force = 0;

		    if (this.trackInfo.getRadiusForLane(carInfo.pieceIndex, carInfo.startLane) > 0) {
		    	cent_force = carInfo.speed * carInfo.speed / this.trackInfo.getRadiusForLane(carInfo.pieceIndex, carInfo.startLane);
			}	

            if (this.isCarCentForceWorrying(cent_force)) {
                throttle = 0;
            }
	    }

	    if (throttle > 1.0 || isNaN(throttle) || throttle == undefined) {
	    	throttle = 1.0;
	    }

        if (throttle < 0.0) {
            throttle = 0;
        }

	    return throttle;
	},

	mayRunTurbo: function(pieceIndex, lane) {
		var nextBendWithDistance = this.trackInfo.getNextBendInfo(pieceIndex, lane);
		if (nextBendWithDistance.distance > 300 && this.trackInfo.isStraight(pieceIndex)) {
			return true;
		}

		return false;
	}

}