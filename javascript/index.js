var net = require("net");
var JSONStream = require('JSONStream');
var logger = require('./logger');
var trackInfo = require('./TrackInfo');
var positionInfo = require('./CarPositionsInfo');
var throttleStrategy = require('./FrictionBasedThrottleStrategy');
var switchingStrategy = require('./ShortLapSwitchingStrategy');

var serverHost = process.argv[2];
var serverPort = process.argv[3];
var botName = process.argv[4];
var botKey = process.argv[5];
var currentPiece = -1;
var currentLane = 0;
var turboAvailable = false;

var carName = 'Articos RT';

console.log("I'm", botName, "and connect to", serverHost + ":" + serverPort);

client = net.connect(serverPort, serverHost, function() {
  var trackName = false;
  if (trackName) {
    return send({
      msgType: 'joinRace',
      data: {
        botId: {
          name: botName,
          key: botKey
        },
        trackName: trackName,
        carCount: 1
      }
    });
  } else {
    return send({
      msgType: 'join',
      data: {
        name: botName,
        key: botKey
      }
    });
  }

});

function send(json) {
  client.write(JSON.stringify(json));
  return client.write('\n');
};

jsonStream = client.pipe(JSONStream.parse());

jsonStream.on('data', function(data) {
  logger.log(data);
  if (data.msgType === 'carPositions') {

    console.log('\n');

    positionInfo.setInfo(data.data);
    var previousCarInfo = positionInfo.getPreviousCarInfo(carName);
    var carInfo = positionInfo.getCarInfo(carName);

    trackInfo.updateAngle(carInfo.angle, carInfo.pieceIndex, carInfo.startLane);

    if (carInfo.lap != previousCarInfo.lap) {
        console.log('----------- NEW LAP: ' + carInfo.lap);
        trackInfo.updateBendInfo();
    }

    if (carInfo.pieceIndex != currentPiece) {     
      currentPiece = carInfo.pieceIndex;
      currentLane = carInfo.startLane;
      console.log('--------- Piece: ' + currentPiece + ' angle: ' + trackInfo.getAngle(carInfo.pieceIndex) + ' lane: ' + carInfo.startLane);
    }

    var switchTo = switchingStrategy.maySwitch(positionInfo);
    if (switchTo !== false) {
      console.log('switching ' + switchTo);

      send({
        msgType: "switchLane",
        data: switchTo
      });
    } else if (turboAvailable && throttleStrategy.mayRunTurbo(carInfo.pieceIndex, carInfo.startLane)) {
      console.log('RUNNING TURBO');
      positionInfo.setTurbo(carName, turboAvailable);
      turboAvailable = false;

      send({
        msgType: "turbo",
        data: 'run run run'
      });
    } else {
      console.log('+ speed: ' + carInfo.speed + " (ac. " + carInfo.acceleration + ") at throttle " + carInfo.throttle + " with angle " + carInfo.angle);
      var throttle = throttleStrategy.getNewThrottle();
      positionInfo.setThrottle(carName, throttle);

      console.log('    SET THROTTLE TO ' + throttle);
      send({
        msgType: "throttle",
        data: throttle
      });
      
      if (trackInfo.isBend(carInfo.pieceIndex)) {
        var fcent = carInfo.speed * carInfo.speed / trackInfo.getRadiusForLane(carInfo.pieceIndex, carInfo.endLane);
        console.log('    Fuerza centrifuga: ' + fcent);
      }
    }

  } else if(data.msgType === 'gameStart') {
    send({
      msgType: "throttle",
      data: 1.0
    });
  } else if(data.msgType === 'crash') {

      console.log(data.data);
      if (data.data.name == carName) {
          trackInfo.crashed(currentPiece, currentLane);
      }

  } else if(data.msgType === 'spawn') {

      send({
          msgType: "throttle",
          data: 1.0
      });

  } else {
    if (data.msgType === 'gameInit') {
      trackInfo.setInfo(data);      
      positionInfo.setTrackInfo(trackInfo);
      throttleStrategy.init(trackInfo, positionInfo);
      switchingStrategy.init(trackInfo);
    }

    if (data.msgType === 'yourCar') {
      positionInfo.setMyCar(data.data.name);
    }

    if (data.msgType === 'turboAvailable') {
      turboAvailable = data.data;
    }

    send({
      msgType: "ping",
      data: {}
    });
  }
});

jsonStream.on('error', function() {
  return console.log("disconnected");
});
