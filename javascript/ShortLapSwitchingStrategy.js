var trackInfo;
var switchOnPiece;

module.exports = {
	
	init: function(trackInfo) {
		this.trackInfo = trackInfo;
		this.switchOnPiece = -1;
	},

	maySwitch: function(carPositions) {
        console.log('may switch?');

		if(carPositions.isChangingLane()) {
            console.log('changing lane');
			this.switchOnPiece = -1;
		}

		var currentPiece = carPositions.getPieceIndex();
		var nextPiece = this.trackInfo.nextPieceIndex(currentPiece);
		if(!this.trackInfo.isSwitch(nextPiece)) {
            console.log('not switch');
			return false;
		}

		if(this.switchOnPiece <= currentPiece) {
			this.switchOnPiece = -1;
		}
		
		if (this.switchOnPiece > -1) {
            console.log('switching');
			return false;
		}

		var currentLane = carPositions.getActualLane();
		var nextSwitch = this.trackInfo.getNextSwitchIndex(currentPiece);
		var nextToNextSwitch = this.trackInfo.getNextSwitchIndex(nextSwitch);

		var lanes = this.trackInfo.getNumberOfLanes();
		var shortLane = 100000000;
		var shortLaneIndex = -1;
		var sameDistance = false;
		for(var i = 0; i < lanes; i++) {
			var distance = this.trackInfo.getDistanceBetween(nextSwitch, nextToNextSwitch, i);
            console.log('distance for lane ' + i + ' is ' + distance);
			if(distance < shortLane){
				sameDistance = false;
				shortLane = distance;
				shortLaneIndex = i;
			} else if(distance == shortLane) {
				sameDistance = true;
			}

		}

		if(sameDistance || shortLaneIndex == currentLane) {
			return false;
		}

		this.switchOnPiece = nextPiece;
		if(shortLaneIndex < currentLane) {
			return 'Left';
		} else if (shortLaneIndex > currentLane) {
			return 'Right';
		}
	}
}